<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Faker\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $faker_id_person = Factory::create('id_ID');
    return [
        'name' => $faker_id_person->name,
        'username' => $faker_id_person->username,
        'email' => $faker_id_person->unique()->email,
        'password' => password_hash('123456', PASSWORD_BCRYPT),
        'nik' => $faker_id_person->nik(),
        'telp' => $faker_id_person->e164PhoneNumber
    ];
});
