<?php

use Illuminate\Database\Migrations\Migration;

class CreateVerifiedTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            CREATE TRIGGER tr_update_verified_after_add_tanggapan 
                AFTER INSERT ON tb_tanggapan FOR EACH ROW
                BEGIN
                    UPDATE tb_pengaduan SET tb_pengaduan.verified = 1 WHERE tb_pengaduan.id = NEW.id_pengaduan;
                END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `tr_update_verified_after_add_tanggapan`');
    }
}
