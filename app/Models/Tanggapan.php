<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tanggapan extends Model
{
    
    protected $table = "tb_tanggapan";

    protected $fillable = ['id_pengaduan', 'tanggapan', 'id_petugas', 'petugas'];

    public function pengaduan() {
        return $this->belongsTo(\App\Models\Pengaduan::class, 'id_pengaduan', 'id');
    }

    public function petugas() {
        return $this->belongsTo(\App\User::class, 'id_petugas', 'id');
    }

}