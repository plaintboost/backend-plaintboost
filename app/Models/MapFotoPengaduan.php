<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MapFotoPengaduan extends Model
{

    protected $table = "map_foto_pengaduan";

    protected $fillable = ['id_pengaduan', 'foto'];

    public function pengaduan() {
        return $this->belongsTo(\App\Models\Pengaduan::class, 'id_pengaduan', 'id');
    }

}