<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pengaduan extends Model
{
    
    protected $table = "tb_pengaduan";

    protected $fillable = ['id_user', 'judul_laporan', 'laporan', 'verified', 'foto', 'user'];

    public function user() {
        return $this->belongsTo(\App\User::class, 'id_user', 'id');
    }

    public function foto() {
        return $this->hasMany(\App\Models\MapFotoPengaduan::class, 'id_pengaduan', 'id');
    }

}