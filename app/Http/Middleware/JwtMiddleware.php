<?php
namespace App\Http\Middleware;

use Closure;
use Exception;
use App\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use App\Http\Controllers\Controller;

class JwtMiddleware extends Controller
{

    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->header('Authorization');
        
        if(!$token) {
            
            return $this->sendFailed(
                ["message" => "Token tidak tersedia"],
                ["errors" => [ "token" => "Token tidak tersedia" ]],
                401
            );
        }
        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch(ExpiredException $e) {
            return $this->sendFailed(
                ["message" => "Token diberikan sudah kadaluarsa"],
                ["errors" => [ "token" => "Token diberikan sudah kadaluarsa" ]],
                400
            );
        } catch(Exception $e) {
            return $this->sendFailed(
                ["message" => "Error saat decode token"],
                ["errors" => [ "token" => "Error saat decode token"]],
                400
            );
        }
        $user = User::find($credentials->sub);
        
        $request->auth = $user;
        return $next($request);
    }

}