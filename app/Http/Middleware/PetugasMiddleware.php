<?php
namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\Controller;

class PetugasMiddleware extends Controller
{

    public function handle($request, Closure $next, $guard = null)
    {
        if($request->auth->role_user != 'admin' && $request->auth->role_user != 'petugas') {
            return $this->sendFailed(
                ["message" => "Anda bukan admin atau petugas"],
                ["errors" => [
                    "user" => "Tidak memiliki akses untuk request ini"
                ]],
                406
            );
        }

        return $next($request);
    }

}