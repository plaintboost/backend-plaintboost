<?php

namespace App\Http\Controllers\Helper;

use Illuminate\Support\Facades\Storage;
use App\User;
use App\Models\MapFotoPengaduan;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class FotoHelper extends Controller {

    private $foto; 
    private $idBarang;
    private $storagePath;
    private $formatPath;
    private $data;

    public function __construct($foto, $idLaporan) {

        $this->foto = $foto;
        $this->idLaporan = $idLaporan;
        $this->formatPath = "/foto-laporan/$idLaporan/";
        $this->storagePath = base_path('public') . "/foto-laporan/$idLaporan";

    }

    private function generate_nama_foto() {

        $result = "";
        do {

            $randomInt = rand(1, 25);
            $randomString = str::random($randomInt);
            $prefix = "LPRN";
            $result = $prefix . $randomString;
            $foto = MapFotoPengaduan::where('foto', $result)->get();

        } while(!$foto->isEmpty());
        return $result;

    }

    public function store() {

        $randomNamaFoto = $this->generate_nama_foto() . "." . $this->foto->getClientOriginalExtension();

        // 1. Store Ke database dalam bentuk nama foto
        $data = array(
            "id_pengaduan" => $this->idLaporan,
            "foto" => $this->formatPath . $randomNamaFoto 
        );
        MapFotoPengaduan::create($data);

        // 2. Store ke local storage
        $specificPath = $this->storagePath . "/" . $this->idBarang;
        $this->formatPath = $this->formatPath . $randomNamaFoto;
        $this->data = $data;
        $this->foto->move($specificPath, $randomNamaFoto);

    }

    public function getSpecificPath() {
        return $this->formatPath;
    }

    public function getData() {
        return $this->data;
    }

    public function delete() {

        // 1. hapus 
        unlink($storagePath . "/$this->idLaporan/$this->foto");

    }

}