<?php
namespace App\Http\Controllers\Auth;

use App\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * Create a new token.
     * 
     * @param  \App\User   $user
     * @return string
     */
    protected function jwt(User $user, $logout = false) {
        $payload = [
            'iss' => "lumen-jwt", 
            'sub' => $user->id, 
            'iat' => time()
        ];
        
        return JWT::encode($payload, env('JWT_SECRET'));
    } 

    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     * 
     * @param  \App\User   $user 
     * @return mixed
     */
    public function authenticate(User $user) {
        $validation = [
            'email'     => 'required|email',
            'password'  => 'required'
        ];
        
        $validator = Validator::make($this->request->all(), $validation);

        // dd($validator->errors()->getMessages()["email"]);

        if($validator->fails()) {

            $errorMessages = $this->getErrorMessages($validator);

            return $this->sendFailed(
                ["message" => "some errors has happened"],
                ["errors" => $errorMessages],
                422
            );
        }
        
        $user = User::where('email', $this->request->input('email'))->first();
        if (!$user) {
            return $this->sendFailed(
                ["message" => "Email tidak tersedia."],
                ["errors" => ["Email tidak tersedia."]],
                400
            );
        }
        
        if (Hash::check($this->request->input('password'), $user->password)) {
            return $this->sendSuccess(
                ["message" => "Berhasil login."], 
                ["token" => $this->jwt($user),
                "profile" => $user]
            );
        }
        
        return $this->sendFailed(
            ["message" => "Email or password is wrong."],
            ["errors" => ["Email or password is wrong."]],
            400
        );
    }

    /**
     * Register a user and return the token 
     * if user done the registration process
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function register(Request $request) {
        $validation = [
            'name' => 'required|string|max:50',
            'username' => 'required|string|max:50',
            'email' => 'required|string|email|unique:App\User,email',
            'password' => 'required|string',
            'nik' => 'required|string|max:20',
            'telp' => 'required|string|max:20|unique:App\User,telp'
        ];

        $validator = Validator::make($request->all(), $validation);

        if($validator->fails()) {

            $errorMessages = $this->getErrorMessages($validator);

            return $this->sendFailed(
                ["message" => "some errors has happened"],
                ["errors" => $errorMessages],
                422
            );
        }

        // kalkulasi umur, harus >= 13 tahun
        $tanggalLahir = ((int) substr($request->nik, 6, 2) > 40) ? (int) substr($request->nik, 6, 2) - 40 : (int) substr($request->nik, 6, 2);
        $bulanLahir = (int) substr($request->nik, 8, 2);

        $nowYear = (int) substr(date("Y"), 2);
        $tahunLahir = ((int) substr($request->nik, 10, 2) < 20) ? "20".substr($request->nik, 10, 2) : "19".substr($request->nik, 10, 2);

        $formatTanggalLahir = new \DateTime("$bulanLahir.$tanggalLahir.$tahunLahir");
        $today = new \DateTime(date('m.d.y'));
        $diff = $today->diff($formatTanggalLahir);
        $umur = $diff->y;
        
        if($umur <= 13) {
            return $this->sendFailed(
                ["message" => "some errors has happened"],
                ["errors" => ["Anda belum cukup umur"]],
                422
            );
        }

        $user = new User();
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->nik = $request->nik;
        $user->telp = $request->telp;

        if($user->save()) {
            return $this->sendSuccess(
                ["message" => "Akun berhasil dibuat"],
                ["profile" => $user]
            );
        } else {
            return $this->sendFailed(
                ["message" => "some errors has happened"],
                ["errors" => ["Akun gagal dibuat"]],
                422
            );
        }
    }
   
}