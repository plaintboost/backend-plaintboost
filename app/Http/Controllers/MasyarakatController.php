<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Helper\FotoHelper;
use App\Models\Pengaduan;
use App\Models\Tanggapan;
use App\Models\MapFotoPengaduan;

class MasyarakatController extends Controller
{
    
    public function getPengaduan(Request $request) {
        $id_user = $request->auth->id;
        if(isset($request->verified)) { 
            $verified_filter = ($request->verified === 'true') ? 1 : 0; 
        }
        $pengaduan = Pengaduan::where('id_user', $id_user)->where('verified', $verified_filter)->paginate(5);

        if(isset($request->q)) {
            if($request->q != "") {
                $pengaduan = Pengaduan::where('id_user', $id_user)->where('judul_laporan', 'like', '%'.$request->q.'%')->paginate(5);
            }
        }

        foreach($pengaduan as $item) {
            $item->user = $item->user->makeHidden('username', 'email', 'nik', 'telp', 'role_user');
        }

        return $this->sendSuccess(
            ["message" => "Berhasil mengambil data pengaduan"], 
            ["complaint" => $pengaduan,
            "total_complaint" => count($pengaduan)]
        );
    }

    public function getDetailPengaduan(Request $request, $id) {
        $id_user = $request->auth->id;
        $pengaduan = Pengaduan::where('id_user', $id_user)->where('id', $id)->get()[0];
        $tanggapan = Tanggapan::where('id_pengaduan', $id)->first();

        if($pengaduan === null) {
            return $this->sendFailed(["message" => "Data tidak ditemukan"]);
        }

        $pengaduan->foto = $pengaduan->foto;
        $pengaduan->user = $pengaduan->user->makeHidden('username', 'email', 'nik', 'telp', 'role_user');

        if($tanggapan !== null) {
            $tanggapan->petugas = $tanggapan->petugas->makeHidden('username', 'email', 'nik', 'telp', 'role_user');
        }

        return $this->sendSuccess(
            ["message" => "Berhasil mengambil data detail pengaduan"], 
            ["complaint" => $pengaduan, 
            "response" => $tanggapan]
        );
    }

    public function postPengaduan(Request $request) {
        $validation = [
            'judul_laporan' => 'required|string|max:100',
            'laporan' => 'required|string',
            'foto.*' => 'image|mimes:jpeg,png,jpg,gif|max:2048'
        ];

        $validator = Validator::make($request->all(), $validation);

        if($validator->fails()) {
            return $this->sendFailed(
                ["message" => "some errors has happened"],
                ["errors" => $validator->errors()]
            );
        }

        $pengaduan = new Pengaduan();
        $pengaduan->id_user = $request->auth->id;
        $pengaduan->judul_laporan = $request->judul_laporan;
        $pengaduan->laporan = $request->laporan;
        
        if(!$pengaduan->save()) {
            return $this->sendFailed(["message" => "some errors has happened"]);
        } 

        $result_foto = [];
        if($request->hasFile('foto')) {
            $foto['foto'] = $request->file('foto');
            for($i = 0; $i < count($foto['foto']); $i++) {
                $fotoPengaduan = new FotoHelper($foto['foto'][$i], $pengaduan->id);
                $fotoPengaduan->store();
                array_push($result_foto, $fotoPengaduan->getData());
            }
        }

        $pengaduan->foto = $result_foto;

        return $this->sendSuccess(
            ["message" => "Berhasil menyimpan data pengaduan"],
            ["complainant" => $request->auth->makeHidden('username', 'email', 'nik', 'telp'),
            "complaint" => $pengaduan]
        );
    }

    public function storeFoto($foto, $idLaporan) {
        
    }

}