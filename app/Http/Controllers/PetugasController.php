<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\Models\Pengaduan;
use App\Models\Tanggapan;
use App\User;

class PetugasController extends Controller
{
    
    public function getPengaduan(Request $request) {
        if(isset($request->verified)) { 
            $verified_filter = ($request->verified === 'true') ? 1 : 0; 
        }

        $pengaduan = Pengaduan::where('verified', $verified_filter)->paginate(5);

        if(isset($request->q)) {
            $pengaduan = Pengaduan::where('verified', $verified_filter)->where('judul_laporan', 'like', '%'.$request->q.'%')->paginate(5);
        }

        foreach($pengaduan as $key => $item) {
            $item->user = $item->user->makeHidden('username', 'email', 'nik', 'telp', 'role_user');
        }

        return $this->sendSuccess(
            ["message" => "Berhasil mengambil data pengaduan"], 
            ["complaint" => $pengaduan,
            "total_complaint" => count($pengaduan)]
        );
    }

    public function getDetailPengaduan($id) {
        try {
            $pengaduan = Pengaduan::where('id', $id)->where('verified', 0)->firstOrFail();
        } catch(\Exception $e) {
            return $this->sendFailed(
                ["message" => $e->getMessage()],
                ["errors" => [
                    "pengaduan" => $e->getMessage()
                ]],
                404
            );
        }

        $pengaduan->foto = $pengaduan->foto;
        $pengaduan->user = $pengaduan->user->makeHidden('username', 'email', 'nik', 'telp', 'role_user');

        return $this->sendSuccess(
            ["message" => "Berhasil mengambil data detail pengaduan"], 
            ["complaint" => $pengaduan]
        );
    }

    public function postTanggapan(Request $request, $id) {
        $validation = [
            'tanggapan' => 'required|string'
        ];

        $validator = Validator::make($request->all(), $validation);

        if($validator->fails()) {
            return $this->sendFailed(
                ["message" => "some errors has happened"],
                ["errors" => $validator->errors()]
            );
        }

        try {
            $pengaduan = Pengaduan::where('id', $id)->where('verified', 0)->firstOrFail();
            $pengaduan->user = $pengaduan->user->makeHidden('username', 'email', 'nik', 'telp');
        } catch(\Exception $e) {
            return $this->sendFailed(
                ["message" => $e->getMessage()],
                ["errors" => [
                    "pengaduan" => $e->getMessage()
                ]],
                404
            );
        }

        $tanggapan = new Tanggapan();
        $tanggapan->id_pengaduan = $id;
        $tanggapan->tanggapan = $request->tanggapan;
        $tanggapan->id_petugas = $request->auth->id;

        if(!$tanggapan->save()) {
            return $this->sendFailed(["message" => "some errors has happened"]);
        } 

        return $this->sendSuccess(
            ["message" => "Berhasil menyimpan data tanggapan"],
            ["response" => $tanggapan->tanggapan,
            "officer" => $request->auth->makeHidden('username', 'email', 'nik', 'telp'),
            "complaint" => $pengaduan]
        );
    }

}