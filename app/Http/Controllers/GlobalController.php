<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Pengaduan;
use App\Models\Tanggapan;
use App\Models\MapFotoPengaduan;

class GlobalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Mengembalikan response untuk mengambil profile user
     * menggunakan jwt token
     *
     * @param  \Illuminate\Http\Request  $request
     * @return App\User
     */
    public function getProfile(Request $request) {
        return $this->sendSuccess(
            ["message" => "Berhasil mengambil data profile"],
            ["profile" => $request->auth]
        );
    }

    public function getDetailPengaduan(Request $request, $id) {
        $pengaduan = Pengaduan::where('id', $id)->get()[0];
        $tanggapan = Tanggapan::where('id_pengaduan', $id)->first();

        if($pengaduan === null) {
            return $this->sendFailed(["message" => "Data tidak ditemukan"]);
        }

        $pengaduan->foto = $pengaduan->foto;
        $pengaduan->user = $pengaduan->user->makeHidden('username', 'email', 'nik', 'telp', 'role_user');

        if($tanggapan !== null) {
            $tanggapan->petugas = $tanggapan->petugas->makeHidden('username', 'email', 'nik', 'telp', 'role_user');
        }

        return $this->sendSuccess(
            ["message" => "Berhasil mengambil data detail pengaduan"], 
            ["complaint" => $pengaduan, 
            "response" => $tanggapan]
        );
    }

    
}
