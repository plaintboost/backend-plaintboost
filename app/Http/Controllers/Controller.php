<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function sendSuccess($message, $data, $resultCode = 200) {
        return response()->json([
            "success" => true,
            "message" => $message,
            "data" => $data
        ], 200);
    }

    public function sendFailed($message, $data = null, $resultCode = 500) {
        return response()->json([
            "success" => false,
            "message" => $message,
            "data" => $data
        ], $resultCode);
    }

    public function getErrorMessages($validator) {
        $errors = [];
        foreach($validator->errors()->getMessages() as $key => $value) {
            $errors += $value;
        }
        return $errors;
    }
}
