<?php

/*
|--------------------------------------------------------------------------
| Route Aplikasi
|--------------------------------------------------------------------------
|
| Di sinilah Anda dapat mendaftarkan semua rute untuk suatu aplikasi.
| Sangat mudah. Cukup beri tahu Lumen URI yang harus ditanggapi
| dan berikan Penutupan untuk menelepon ketika URI itu diminta.
|
*/

/**
 * Prefix untuk API
 * 
 * @return json data
 */
$router->group(['prefix' => 'api'], function($router) {

    /**
     * Fungsi authentikasi
     * 
     * Fungsi authentikasi ini dapat digunakan oleh 3 role
     * yaitu role masyarakat, role petugas, dan role admin. 
     * 
     * 1. Login
     * 2. Registrasi user baru (masyarakat)
     */
    $router->post('/auth/login', 'Auth\AuthController@authenticate');
    $router->post('/auth/register', 'Auth\AuthController@register');


    /**
     * Route group untuk endpoint yang membutuhkan
     * middleware json web token (jwt)
     */
    $router->group(['middleware' => 'jwt.auth'], function($router) {

        /**
         * Fungsi umum
         * 
         * Fungsi yang dapat digunakan oleh semua jenis role
         * untuk mengakses berbagai hal umum
         * 
         * 1. Get profile data
         */
        $router->get('/get-profile', 'GlobalController@getProfile');
        $router->get('/pengaduan/{id}', 'GlobalController@getDetailPengaduan');

        /**
         * Fungsi masyarakat
         * 
         * Fungsi yang dapat digunakan hanya oleh masyarakat
         * untuk mengakses 
         * 
         * 1. Get list pengaduan
         * 2. Get detail list pengaduan
         * 3. Post data pengaduan
         */
        $router->group(['prefix' => 'masyarakat'], function($router) {
            $router->get('/pengaduan', 'MasyarakatController@getPengaduan');
            $router->get('/pengaduan/{id}', 'MasyarakatController@getDetailPengaduan');
            $router->post('/pengaduan', 'MasyarakatController@postPengaduan');
        });

        /**
         * Fungsi petugas
         * 
         * Fungsi yang dapat digunakan hanya oleh masyarakat
         * untuk mengakses 
         * 
         * 1. Get list pengaduan
         * 2. Get detail list pengaduan
         * 3. Post tanggapan + verifikasi
         */
        $router->group(['prefix' => 'petugas', 'middleware' => ['petugas']], function($router) {
            $router->get('/pengaduan', 'PetugasController@getPengaduan');
            $router->get('/pengaduan/{id}', 'PetugasController@getDetailPengaduan');
            $router->post('/pengaduan/{id}/tanggapan', 'PetugasController@postTanggapan');
        });

    });
});